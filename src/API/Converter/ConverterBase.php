<?php

namespace Drupal\go_wkhtmltox\API\Converter;

/**
 * Defines the go_wkhtmltox converter.
 */
abstract class ConverterBase implements ConverterInterface {

  /**
   * The converter options array.
   *
   * @var array
   */
  protected $options = [];

  /**
   * The conversion method.
   *
   * @var string
   */
  protected $convertTo;

  /**
   * The valid and recognized converter options.
   *
   * @var array
   */
  protected $validOptions = [];

  /**
   * {@inheritdoc}
   */
  public function getConvertTo() {
    return $this->convertTo;
  }

  /**
   * {@inheritdoc}
   */
  public function getOption($name) {
    return $this->getOptions()[$name] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setOption($name, $value = NULL) {
    $this->assertOption($name);

    $params = $this->getOptions();
    $params[$name] = $value;
    $this->setOptions($params);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOptions() {
    return $this->options;
  }

  /**
   * {@inheritdoc}
   */
  public function setOptions(array $options = []) {
    $this->options = [];
    foreach ($options as $name => $value) {
      $this->assertOption($name);

      // @todo Validate values.
      $this->options[$name] = $value;
    }

    return $this->options;
  }

  /**
   * Asserts that the given option name is valid.
   *
   * @param string $name
   *   The option name.
   */
  protected function assertOption($name) {
    if (!in_array($name, $this->validOptions)) {
      throw new \InvalidArgumentException(sprintf('Unknown converter option "%s".', $name));
    }
  }

}
