### SUMMARY
This module integrates Drupal with [Go-WkhtmltoX](https://github.com/sbitio/docker-wkhtmltox).

### INSTALLATION
Install as usual, see [Installing Drupal 8
Modules](https://www.drupal.org/node/1897420) or [Installing modules' Composer dependencies](https://www.drupal.org/node/2627292) for further information.

### CONFIGURATION
Add your endpoint to your settings.php
```php
$config['go_wkhtmltox.settings']['endpoint'] = 'http://wkhtmltox:8080';
```

### PDF EXAMPLE
```php
use Drupal\go_wkhtmltox\API\Converter\ToPdfConverter;
use Drupal\go_wkhtmltox\API\Fetcher\DataFetcher;

$pdf = [
 '#type' => 'inline_template',
 '#template' => '<h1>Hello world!</h1>',
];

$pdf_data = (string) \Drupal::service('renderer')->renderPlain($pdf);
$converter = new ToPdfConverter();
$converter->setOption('extend', [
  'dpi' => '300',
  'image-dpi' => '300',
  'image-quality' => '100',
  'page-width' => '210mm',
  'page-height' => '297mm',
]);
$fetcher = new DataFetcher();
$fetcher->setParam('data', $pdf_data);
$pdf_file = \Drupal::service('go_wkhtmltox.client')->convert($converter, $fetcher);
```

### IMAGE EXAMPLE
```php
use Drupal\go_wkhtmltox\API\Converter\ToImageConverter;
use Drupal\go_wkhtmltox\API\Fetcher\DataFetcher;

$pdf = [
  '#type' => 'inline_template',
  '#template' => '<h1>Hello world!</h1>',
];

$pdf_data = (string) \Drupal::service('renderer')->renderPlain($pdf);
$converter = new ToImageConverter();
$converter->setOption('format', 'png');
// Sets the minimum value, it'll be auto-calculated.
$converter->setOption('width', 0);
$converter->setOption('extend', [
  'transparent' => '--transparent',
  // This option isn't added by itself.
  // @see https://github.com/gogap/go-wkhtmltox/issues/8
  'quality' => '0',
]);
$fetcher = new DataFetcher();
$fetcher->setParam('data', $pdf_data);
$pdf_file = \Drupal::service('go_wkhtmltox.client')->convert($converter, $fetcher);
```

### SPONSORS
- [Fundación UNICEF Comité Español](https://www.unicef.es)

### CONTACT
Developed and maintained by Cambrico (http://cambrico.net).

Get in touch with us for customizations and consultancy:
http://cambrico.net/contact

#### Current maintainers:
- Pedro Cambra [(pcambra)](https://www.drupal.org/u/pcambra)
- Manuel Egío [(facine)](https://www.drupal.org/u/facine)
