<?php

namespace Drupal\go_wkhtmltox\API\Converter;

/**
 * Defines the go_wkhtmltox ToPdf converter.
 */
class ToPdfConverter extends ConverterBase implements ConverterInterface {

  /**
   * {@inheritdoc}
   */
  protected $convertTo = 'pdf';

  /**
   * {@inheritdoc}
   */
  protected $validOptions = [
    'uri', 'no_collate', 'copies', 'gray_scale', 'low_quality',
    'orientation', 'page_size', 'print_media_type', 'extend',
  ];

}
