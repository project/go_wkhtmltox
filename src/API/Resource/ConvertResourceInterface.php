<?php

namespace Drupal\go_wkhtmltox\API\Resource;

/**
 * Defines an interface for the go_wkhtmltox convert resource class.
 */
interface ConvertResourceInterface extends ResourceInterface {}
