<?php

namespace Drupal\go_wkhtmltox;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\go_wkhtmltox\API\Converter\ConverterInterface;
use Drupal\go_wkhtmltox\API\Fetcher\FetcherInterface;
use Drupal\go_wkhtmltox\API\Resource\ConvertResource;
use Drupal\go_wkhtmltox\API\Resource\PingResource;

/**
 * Defines the go_wkhtmltox client.
 */
class GoWkhtmltoX implements GoWkhtmltoXInterface {

  /**
   * The config factory to use.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface|null
   */
  protected $configFactory;

  /**
   * Construct a new GoWkhtmltoX object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory to use.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function ping() {
    $status = FALSE;

    try {
      $ping = new PingResource();
      $response = $ping->get($this->getUrl());

      $status = $response->isValid();
    }
    catch (\Exception $exception) {
      watchdog_exception('go_wkhtmltox', $exception);
    }

    return $status;
  }

  /**
   * {@inheritdoc}
   */
  public function convert(ConverterInterface $converter, FetcherInterface $fetcher = NULL) {
    try {
      $convert = new ConvertResource($converter, $fetcher, $this->configFactory);
      $response = $convert->get($this->getUrl());

      if ($response->isValid()) {
        return base64_decode($response->getResult()['data']);
      }
      else {
        throw new \Exception(sprintf('Error code "%s": %s.', $response->getCode(), $response->getMessage()));
      }
    }
    catch (\Exception $exception) {
      watchdog_exception('go_wkhtmltox', $exception);
    }

    return NULL;
  }

  /**
   * Get the endpoint URL.
   *
   * @return string
   *   The endpoint URL.
   */
  protected function getUrl() {
    // @todo Add form configuration and validation.
    return $this->configFactory->get('go_wkhtmltox.settings')->get('endpoint') . $this->configFactory->get('go_wkhtmltox.settings')->get('path');
  }

}
