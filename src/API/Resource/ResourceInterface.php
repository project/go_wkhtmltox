<?php

namespace Drupal\go_wkhtmltox\API\Resource;

/**
 * Defines an interface for the go_wkhtmltox resource class.
 */
interface ResourceInterface {

  /**
   * Returns the webservice response.
   *
   * @param string $path
   *   The endpoint path path.
   *
   * @return \Drupal\go_wkhtmltox\API\Response\Response
   *   The webservice response.
   */
  public function get($path);

}
