<?php

namespace Drupal\go_wkhtmltox\API\Fetcher;

/**
 * Defines an interface for the go_wkhtmltox fetcher class.
 */
interface FetcherInterface {

  /**
   * Returns the fetcher name.
   *
   * @return string
   *   The fetcher name.
   */
  public function getName();

  /**
   * Returns the specific fetcher parameter value.
   *
   * @param string $name
   *   The parameter name.
   *
   * @return string
   *   The specific fetcher parameter value.
   */
  public function getParam($name);

  /**
   * Sets the specific fetcher parameter value.
   *
   * @param string $name
   *   The parameter name.
   * @param mixed $value
   *   The parameter value.
   *
   * @return $this
   *
   * @throws \InvalidArgumentException
   */
  public function setParam($name, $value = NULL);

  /**
   * Returns the fetcher params values.
   *
   * @return array
   *   The fetcher params values.
   */
  public function getParams();

  /**
   * Sets the fetcher params values.
   *
   * @param array $params
   *   The fetcher params values.
   *
   * @return $this
   *
   * @throws \InvalidArgumentException
   */
  public function setParams(array $params = []);

}
