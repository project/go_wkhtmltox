<?php

namespace Drupal\go_wkhtmltox\API\Converter;

/**
 * Defines an interface for the go_wkhtmltox ToImage converter class.
 */
interface ToImageInterface extends ConverterInterface {}
