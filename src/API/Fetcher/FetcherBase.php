<?php

namespace Drupal\go_wkhtmltox\API\Fetcher;

/**
 * Defines the go_wkhtmltox fetcher.
 */
abstract class FetcherBase implements FetcherInterface {

  /**
   * The fetcher name.
   *
   * @var string
   */
  protected $name;

  /**
   * The fetcher parameters array.
   *
   * @var array
   */
  protected $params = [];

  /**
   * The valid and recognized fetcher options.
   *
   * @var array
   */
  protected $validParams = [];

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->name;
  }

  /**
   * {@inheritdoc}
   */
  public function getParam($name) {
    return $this->getParams()[$name] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setParam($name, $value = NULL) {
    $this->assertParameter($name);

    $params = $this->getParams();
    $params[$name] = $value;
    $this->setParams($params);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getParams() {
    return $this->params;
  }

  /**
   * {@inheritdoc}
   */
  public function setParams(array $params = []) {
    $this->params = [];
    foreach ($params as $name => $value) {
      $this->assertParameter($name);

      // Make sure data is base64 encoded.
      if ($name == 'data' && base64_encode(base64_decode($value, TRUE)) !== $value) {
        $value = base64_encode($value);
      }
      // @todo Validate values.
      $this->params[$name] = $value;
    }

    return $this;
  }

  /**
   * Asserts that the given parameter name is valid.
   *
   * @param string $name
   *   The parameter name.
   */
  protected function assertParameter($name) {
    if (!in_array($name, $this->validParams)) {
      throw new \InvalidArgumentException(sprintf('Unknown fetcher parameter "%s".', $name));
    }
  }

}
