<?php

namespace Drupal\go_wkhtmltox\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Defines a form that configures go_wkhtmltox settings.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'go_wkhtmltox_admin_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'go_wkhtmltox.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Request $request = NULL) {
    $go_wkhtmltox_config = $this->config('go_wkhtmltox.settings');

    $form['endpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Endpoint'),
      '#description' => $this->t('Endpoint Go-wkhtmltoX service'),
      '#default_value' => $go_wkhtmltox_config->get('endpoint'),
      '#required' => TRUE,
    ];

    $form['path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Path'),
      '#description' => $this->t('Path Go-wkhtmltoX service'),
      '#default_value' => $go_wkhtmltox_config->get('path'),
      '#required' => TRUE,
    ];

    $form['timeout'] = [
      '#type' => 'number',
      '#title' => $this->t('Timeout'),
      '#description' => $this->t('Guzzle timeout'),
      '#default_value' => $go_wkhtmltox_config->get('timeout') ?? 15,
      '#required' => TRUE,
    ];

    $form['connect_timeout'] = [
      '#type' => 'number',
      '#title' => $this->t('Connect timeout'),
      '#description' => $this->t('Guzzle connect_timeout'),
      '#default_value' => $go_wkhtmltox_config->get('connect_timeout') ?? 1,
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->config('go_wkhtmltox.settings')
      ->set('endpoint', $values['endpoint'])
      ->set('path', $values['path'])
      ->set('timeout', $values['timeout'])
      ->set('connect_timeout', $values['connect_timeout'])
      ->save();

    parent::submitForm($form, $form_state);
  }

}
