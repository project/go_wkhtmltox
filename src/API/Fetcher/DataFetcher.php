<?php

namespace Drupal\go_wkhtmltox\API\Fetcher;

/**
 * Defines the go_wkhtmltox Data fetcher.
 */
class DataFetcher extends FetcherBase implements DataFetcherInterface {

  /**
   * {@inheritdoc}
   */
  protected $name = 'data';

  /**
   * {@inheritdoc}
   */
  protected $validParams = ['url', 'data'];

}
