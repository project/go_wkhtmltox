<?php

namespace Drupal\go_wkhtmltox\API\Resource;

/**
 * Defines an interface for the go_wkhtmltox ping resource class.
 */
interface PingResourceInterface extends ResourceInterface {}
