<?php

namespace Drupal\go_wkhtmltox;

use Drupal\go_wkhtmltox\API\Converter\ConverterInterface;
use Drupal\go_wkhtmltox\API\Fetcher\FetcherInterface;

/**
 * Defines an interface for the go_wkhtmltox client class.
 */
interface GoWkhtmltoXInterface {

  /**
   * Returns the webservice status.
   *
   * @return bool
   *   TRUE if the webservice is available.
   */
  public function ping();

  /**
   * Returns the converted file.
   *
   * @param \Drupal\go_wkhtmltox\API\Converter\ConverterInterface $converter
   *   The converter object.
   * @param \Drupal\go_wkhtmltox\API\Fetcher\FetcherInterface|null $fetcher
   *   The fetcher object, default http.
   *
   * @return \Symfony\Component\HttpFoundation\BinaryFileResponse|null
   *   The converted file.
   */
  public function convert(ConverterInterface $converter, FetcherInterface $fetcher = NULL);

}
