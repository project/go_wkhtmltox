<?php

namespace Drupal\go_wkhtmltox\API\Resource;

use GuzzleHttp\Client;

/**
 * Defines the go_wkhtmltox resource.
 */
abstract class ResourceBase implements ResourceInterface {

  /**
   * The HTTP Client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * The resource path.
   *
   * @var string
   */
  protected $resourcePath;

  /**
   * Constructs a new ResourceBase object.
   */
  public function __construct() {
    $this->httpClient = new Client();
  }

  /**
   * Returns the resource URL.
   *
   * @param string $path
   *   The endpoint path.
   *
   * @return string
   *   The absolute resource URL.
   */
  protected function getUrl($path) {
    return $path . $this->resourcePath;
  }

}
