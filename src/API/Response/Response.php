<?php

namespace Drupal\go_wkhtmltox\API\Response;

/**
 * Defines the go_wkhtmltox response.
 */
class Response implements ResponseInterface {

  /**
   * The error code.
   *
   * @var int
   */
  protected $code;

  /**
   * The error message.
   *
   * @var string
   */
  protected $message;

  /**
   * The conversion result.
   *
   * @var string
   */
  protected $result;

  /**
   * Response constructor.
   *
   * @param string $result
   *   The conversion result.
   * @param int $code
   *   The error code.
   * @param string|null $message
   *   The error message.
   */
  public function __construct($result, $code = 0, $message = NULL) {
    $this->code = $code;
    $this->message = $message;
    $this->result = $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getCode() {
    return $this->code;
  }

  /**
   * {@inheritdoc}
   */
  public function getMessage() {
    return $this->message;
  }

  /**
   * {@inheritdoc}
   */
  public function getResult() {
    return $this->result;
  }

  /**
   * {@inheritdoc}
   */
  public function isValid() {
    return empty($this->code);
  }

  /**
   * {@inheritdoc}
   */
  public static function create($result, $code = 0, $message = NULL) {
    return new static($result, $code, $message);
  }

}
