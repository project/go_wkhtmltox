<?php

namespace Drupal\go_wkhtmltox\API\Converter;

/**
 * Defines the go_wkhtmltox ToImage converter.
 */
class ToImageConverter extends ConverterBase implements ConverterInterface {

  /**
   * {@inheritdoc}
   */
  protected $convertTo = 'image';

  /**
   * {@inheritdoc}
   */
  protected $validOptions = [
    'uri', 'crop', 'format', 'quality',
    'width', 'height', 'extend',
  ];

}
