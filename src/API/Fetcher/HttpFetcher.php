<?php

namespace Drupal\go_wkhtmltox\API\Fetcher;

/**
 * Defines the go_wkhtmltox HTTP fetcher.
 */
class HttpFetcher extends FetcherBase implements HttpFetcherInterface {

  /**
   * {@inheritdoc}
   */
  protected $name = 'http';

  /**
   * {@inheritdoc}
   */
  protected $validParams = ['url', 'method', 'headers', 'data', 'replace'];

}
