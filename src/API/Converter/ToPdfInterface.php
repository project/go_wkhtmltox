<?php

namespace Drupal\go_wkhtmltox\API\Converter;

/**
 * Defines an interface for the go_wkhtmltox ToPdf converter class.
 */
interface ToPdfInterface extends ConverterInterface {}
