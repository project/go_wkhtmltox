<?php

namespace Drupal\go_wkhtmltox\API\Converter;

/**
 * Defines an interface for the go_wkhtmltox converter class.
 */
interface ConverterInterface {

  /**
   * Returns the conversion type.
   *
   * @return string
   *   The conversion type.
   */
  public function getConvertTo();

  /**
   * Returns the specific converter option value.
   *
   * @param string $name
   *   The option name.
   *
   * @return string
   *   The specific converter option value.
   */
  public function getOption($name);

  /**
   * Sets the specific converter option value.
   *
   * @param string $name
   *   The option name.
   * @param mixed $value
   *   The option value.
   *
   * @return $this
   *
   * @throws \InvalidArgumentException
   */
  public function setOption($name, $value = NULL);

  /**
   * Returns the converter options values.
   *
   * @return array
   *   The converter options values.
   */
  public function getOptions();

  /**
   * Sets the converter options values.
   *
   * @param array $options
   *   The converter options values.
   *
   * @return $this
   *
   * @throws \InvalidArgumentException
   */
  public function setOptions(array $options = []);

}
