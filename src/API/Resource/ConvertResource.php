<?php

namespace Drupal\go_wkhtmltox\API\Resource;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\NestedArray;
use Drupal\go_wkhtmltox\API\Converter\ConverterInterface;
use Drupal\go_wkhtmltox\API\Fetcher\DataFetcherInterface;
use Drupal\go_wkhtmltox\API\Fetcher\FetcherInterface;
use Drupal\go_wkhtmltox\API\Fetcher\HttpFetcher;
use Drupal\go_wkhtmltox\API\Fetcher\HttpFetcherInterface;
use Drupal\go_wkhtmltox\API\Response\Response;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the go_wkhtmltox convert resource.
 */
class ConvertResource extends ResourceBase implements ConvertResourceInterface {

  /**
   * The converter object.
   *
   * @var \Drupal\go_wkhtmltox\API\Converter\ConverterInterface
   */
  protected $converter;

  /**
   * The fetcher object.
   *
   * @var \Drupal\go_wkhtmltox\API\Fetcher\FetcherInterface
   */
  protected $fetcher;

  /**
   * The resource path.
   *
   * @var string
   */
  protected $resourcePath = '/convert';

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a new Request object.
   *
   * @param \Drupal\go_wkhtmltox\API\Converter\ConverterInterface $converter
   *   The converter object.
   * @param \Drupal\go_wkhtmltox\API\Fetcher\FetcherInterface|null $fetcher
   *   The fetcher object, default http.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ConverterInterface $converter, FetcherInterface $fetcher = NULL, ConfigFactoryInterface $config_factory) {
    parent::__construct();

    $this->converter = $converter;
    $this->fetcher = $fetcher;
    $this->configFactory = $config_factory->get('go_wkhtmltox.settings');

    if (!$fetcher instanceof FetcherInterface) {
      $this->fetcher = new HttpFetcher();
    }

    // Validate configuration.
    if ($fetcher instanceof HttpFetcherInterface &&
      (empty($fetcher->getParam('url')) || empty($converter->getOption('uri')))) {
      throw new \InvalidArgumentException(sprintf('Missing "url" parameter.'));
    }
    elseif ($fetcher instanceof DataFetcherInterface && empty($fetcher->getParam('data'))) {
      throw new \InvalidArgumentException(sprintf('Missing "data" parameter.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function get($path) {
    try {
      // @todo Move the request method to a field.
      $response = $this->httpClient->request('POST', $this->getUrl($path), $this->buildRequestOptions());
      $json = $response->getBody();
      $result = Json::decode($json);

      return new Response($result['result'], $result['code'], $result['message']);
    }
    catch (\Exception $exception) {
      watchdog_exception('go_wkhtmltox', $exception);

      return new Response(NULL, 999, $exception->getMessage());
    }
  }

  /**
   * Returns the request options to apply.
   *
   * @return array
   *   The request options to apply.
   */
  protected function buildRequestOptions() {
    $options = [
      'verify' => FALSE,
      'timeout' => $this->configFactory->get('timeout') ?? 15,
      'connect_timeout' => $this->configFactory->get('connect_timeout') ?? 1,
      'headers' => [
        'Accept-Encoding' => 'gzip',
        'Cache-Control' => 'no-cache',
        'Content-Type' => 'application/json; charset=UTF-8',
      ],
      'body' => [
        'to' => $this->converter->getConvertTo(),
        'fetcher' => [
          'name' => $this->fetcher->getName(),
          'params' => $this->fetcher->getParams(),
        ],
        'converter' => $this->converter->getOptions(),
      ],
    ];

    $options['body'] = Json::encode(NestedArray::filter($options['body'], function ($value) {
      return $value !== '';
    }));

    return $options;
  }

}
