<?php

namespace Drupal\go_wkhtmltox\API\Resource;

use Drupal\Component\Serialization\Json;
use Drupal\go_wkhtmltox\API\Response\Response;

/**
 * Defines the go_wkhtmltox ping resource.
 */
class PingResource extends ResourceBase implements ConvertResourceInterface {

  /**
   * The resource path.
   *
   * @var string
   */
  protected $resourcePath = '/ping';

  /**
   * {@inheritdoc}
   */
  public function get($path) {
    try {
      // @todo Move the request method to a field.
      $response = $this->httpClient->request('GET', $this->getUrl($path), $this->buildRequestOptions());
      $json = $response->getBody();
      $result = Json::decode($json);

      if ($response->getStatusCode() == 200) {
        return new Response($result);
      }
      else {
        throw new \Exception(sprintf('Error code "%s": %s.', $response->getStatusCode(), $response->getReasonPhrase()));
      }
    }
    catch (\Exception $exception) {
      watchdog_exception('go_wkhtmltox', $exception);

      return new Response(NULL, 999, $exception->getMessage());
    }
  }

  /**
   * Returns the request options to apply.
   *
   * @return array
   *   The request options to apply.
   */
  protected function buildRequestOptions() {
    $options = [
      'verify' => FALSE,
      // @todo Allow to configure the timeout.
      'timeout' => 5,
      'headers' => [
        'Accept-Encoding' => 'gzip',
        'Cache-Control' => 'no-cache',
        'Content-Type' => 'application/json; charset=UTF-8',
      ],
    ];

    return $options;
  }

}
