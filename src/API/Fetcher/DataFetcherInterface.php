<?php

namespace Drupal\go_wkhtmltox\API\Fetcher;

/**
 * Defines an interface for the go_wkhtmltox Data fetcher class.
 */
interface DataFetcherInterface extends FetcherInterface {}
