<?php

namespace Drupal\go_wkhtmltox\API\Response;

/**
 * Defines an interface for the go_wkhtmltox response class.
 */
interface ResponseInterface {

  /**
   * Returns the error code.
   *
   * @return int
   *   The error code.
   */
  public function getCode();

  /**
   * Returns the error message.
   *
   * @return int
   *   The error message.
   */
  public function getMessage();

  /**
   * Returns the conversion data result.
   *
   * @return int
   *   The error code.
   */
  public function getResult();

  /**
   * Determines if the response is valid.
   *
   * @return bool
   *   The validation result.
   */
  public function isValid();

  /**
   * Factory method for chainability.
   *
   * @param string $result
   *   The conversion result.
   * @param int $code
   *   The error code.
   * @param string|null $message
   *   The error message.
   *
   * @code
   *   return Response::create($result);
   * @endCode
   *
   * @return $this
   */
  public static function create($result, $code = 0, $message = NULL);

}
