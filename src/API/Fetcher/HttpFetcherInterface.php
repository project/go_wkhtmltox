<?php

namespace Drupal\go_wkhtmltox\API\Fetcher;

/**
 * Defines an interface for the go_wkhtmltox HTTP fetcher class.
 */
interface HttpFetcherInterface extends FetcherInterface {}
